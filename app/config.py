from instance import instance
import os

CONFIG = dict(
        ROOT_DIR = os.path.abspath(os.path.dirname(__file__)),
        #app_dir = Flask.root_path,
        DATA_DIR = 'data',
        STORAGE_DIR = 'storage',
        UPLOADS_DIR = 'uploads',

        # Secret key for session management. You can generate random strings here:
        # https://randomkeygen.com/
        SECRET_KEY = 'secret!',
        )

CONFIG.update(
        # Connect to the database
        #SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(CONFIG['ROOT_DIR'], 'database.db'),
        )

# Flask-SocketIO
CONFIG.update(
        # to make the app choose
        async_mode = None,
        thread = None,
        )


if instance.ISDEV:
    CONFIG.update(
        TITLE   = "belai",
        PORT    = 2001,
        HOST    = 'localhost',
        VERBOSE = True,
        DEBUG   = True,
        )
else:
    CONFIG.update(
        TITLE   = "belai",
        PORT    = 5000,
        # has to be 5000 for AWS
        HOST    = '0.0.0.0',
        VERBOSE = False,
        DEBUG   = False,
        )

#TODO for d in dirnames.values():
for d in ['ROOT_DIR', 'DATA_DIR', 'STORAGE_DIR', 'UPLOADS_DIR']:
    os.makedirs(CONFIG[d], exist_ok=True)

