import subprocess as sp
import belai
import re
import colormap
from seaborn import palettes

def whiten_palette(palette, shift=0.1):
    def whiten(color, shift=0.1):
        return (min(color[0]+shift, 1), min(color[1]+shift, 1), min(color[2]+shift, 1))
    return palettes._ColorPalette([whiten(c) for c in palette])



def highlight_matches(string, highlight_list):
    '''
    :param string: string in which to highlight patterns
    :param highlight_list: list of patterns to match
    '''


    css_colors = whiten_palette(palettes.color_palette('hls', len(highlight_list))).as_hex()

    highlight_list = highlight_list[:len(css_colors)]
    color_mapp = {h:c for h,c in zip(highlight_list, css_colors)}

    modified_string = string

    shift = 0
    for match in re.finditer("|".join(highlight_list), string):

        start = match.start()
        end = match.end()
        matched = match.group()
        color = color_mapp[matched]
        #TODO: show where the information (text, string) comes from: GO, Entrez etc
        # also add how many genes share this match
        #info_source = ""

        o = [   modified_string[:start+shift],
                modified_string[start+shift:end+shift],
                modified_string[end+shift:]
                ]

        ##dropdown = """<span class="FlagIssue-infobox {colour}">{matched}</span>"""
        ##            {dropdown}

        #replacement = """
        #        <div class="FlagIssue {css_flag}">
        #            <div class="FlagIssue-content {css_flag}">
        #                <p>
        #                Issue type: {info_source}
        #                </p>
        #            </div>
        #        </div>
        #        """

        ##dropdown = dropdown.format(colour = colour, matched=o[1])
        #repl = replacement.format(pattern=o[1], css_flag = css_flag,
        #                            info_source=info_source,
        #                            #dropdown=dropdown
        #                            )

        repl = '<span style="background:'+ str(color) +'">' + str(o[1]) + '</span>'

        shift = shift + (len(repl) - len(o[1]))
        modified_string  = o[0] + repl + o[2]

    return modified_string


def process_entity_list(entity_list, get=['common_concepts', 'set_enrichment']):
    #use_cols = data.biomart['gene_description_columns'][2:3]
    maxrow = 10
    use_cols = ['Gene description', 'GO term definition', 'GO term name']
    o = {}

    if "common_concepts" in get:
        elist  = [0] + [e.input_identifier for e in entity_list]
        elist2 = [0] + [e.resolved_identifier for e in entity_list]
        #elist3 = [0] + [", ".join(list(e.direct_refs['Gene stable ID'])) for e in entity_list]
        elist3 = [0]
        elist4 = [0] + [highlight_matches(
                e.text_from_descriptions(use_cols), entity_list.common_concepts(colnames=use_cols)
                )
                for e in entity_list]
        idx = [i for i in range(len(elist)-1)]
        testitem1 = elist4[1]

        common_concepts = highlight_matches(", ".join(entity_list.common_concepts(colnames = use_cols)),
            entity_list.common_concepts(colnames = use_cols))

        o.update({
            'idx' : idx, 'entity_list' : elist, 'list2' : elist2,
            'list3' : elist3, 'list4' : elist4, 'testitem1':testitem1,
            'common_concepts' : common_concepts,
            })

    if "set_enrichment" in get:
        #typ = [e.typ for e in entity_list].count...
        # if type == "gene":

        gsea = [entity_list.gsea_kegg()[:maxrow].to_html(index=False),
                entity_list.gsea_wikipathways()[:maxrow].to_html(index=False),
                entity_list.gsea_go_process()[:maxrow].to_html(index=False),
                entity_list.gsea(gene_sets=["Aging_Perturbations_from_GEO_down", "Aging_Perturbations_from_GEO_up"])[:maxrow].to_html(index=False),
                entity_list.gsea(gene_sets=["Drug_Perturbations_from_GEO_down", "Drug_Perturbations_from_GEO_up"])[:maxrow].to_html(index=False),
                ]
        sp.Popen(["rm", "-r", "Enrichr"])
        gsea_titles = ["KEGG 2019 (pathway analysis)", "WikiPathways (pathway analysis)", "GO Biological Process", "Aging perturbations from GEO", "Drug perturbations from GEO"]

        o.update({
                'tables' : gsea, 'tables_titles' : ['']+gsea_titles,
                })
    return o

