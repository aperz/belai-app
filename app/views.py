
#import matplotlib
#matplotlib.use('Agg')
import os
import pandas as pd
pd.set_option('display.max_colwidth', -1)

from flask import Flask, render_template, request, session, send_file
#from flask_socketio import SocketIO, emit
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
import logging
from logging import Formatter, FileHandler
from forms import *


import belai
from belai import cheats
from belai import data
import aux

'''
import matplotlib.pyplot as plt
import seaborn as sns

import pandas as pd
import re
import base64
from io import StringIO, BytesIO
import datetime
from werkzeug import secure_filename
import subprocess as sp

pd.set_option('display.max_colwidth', 1000)

# cache
from tempfile import mkdtemp
from joblib import Memory
cachedir = mkdtemp()
memory = Memory(cachedir='cache', verbose=0)
'''


from config import CONFIG
app = Flask(__name__)
app.config.update(CONFIG)

socketio = SocketIO(app, async_mode=CONFIG['async_mode'])

from threading import Lock
thread = CONFIG['thread']
thread_lock = Lock()



# =====
# VIEWS
# =====

@app.route('/')
def home():
    return render_template('pages/home.html')


@app.route('/about', methods = ['POST', 'GET'])
def about():
    return render_template('pages/about.html')


@app.route('/file_issue', methods = ['POST', 'GET'])
def file_issue():
    return render_template('pages/file_issue.html')


@app.route('/quick_start', methods = ['GET', 'POST'])
def quick_start():
    if not request.method == 'POST':
        print("=== Something went wrong.")
        return render_template('pages/simple_text.html',
                    text = ['Something went wrong.']
                    )
    else:
        # TODO: rewrite
        L = belai.relationship.GeneList(cheats.get_glist4())
        res = aux.process_entity_list(L)#, get=['common_concepts'])
        return render_template("pages/results.html", **res)


@app.route("/paste_text", methods=['POST', 'GET'])
def paste_text():
    if not request.method == 'POST':
        print("=== Something went wrong.")
        return render_template('pages/simple_text.html',
                    text = ['Something went wrong.']
                    )
    else:
        text = request.form['text']
        return results(text)

@app.route('/results', methods = ['GET', 'POST'])
def results(query):
    if not request.method == 'POST':
        print("=== Something went wrong.")
        return render_template('pages/simple_text.html',
                    text = ['Something went wrong.']
                    )
    else:
        name_list = [i.strip() for i in query.split("\n")]
        #FIXME: assumes genes
        L = [belai.entity.Gene(e) for e in name_list]
        L = [g for g in L if g.resolved_identifier != None]
        L = belai.relationship.GeneList(L)

        res = aux.process_entity_list(L)#, get=["common_concepts"])
        return render_template("pages/results.html", **res)



"""
@socketio.on('my event', namespace='/test')
def test_message(message):
    emit('my response', {'data': message['data']})

@socketio.on('my broadcast event', namespace='/test')
def test_message(message):
    emit('my response', {'data': message['data']}, broadcast=True)

@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected'})

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')
"""

'''
# Error handlers.
@app.errorhandler(500)
def internal_error(error):
    #db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404
    '''
